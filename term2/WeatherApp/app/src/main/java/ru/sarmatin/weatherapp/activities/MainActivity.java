package ru.sarmatin.weatherapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.sarmatin.weatherapp.App;
import ru.sarmatin.weatherapp.R;
import ru.sarmatin.weatherapp.models.DayTemp;
import ru.sarmatin.weatherapp.models.ForecastInfo;
import ru.sarmatin.weatherapp.service.RequestService;

public class MainActivity extends AppCompatActivity {


    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    private ForecastInfo info;
    private RequestServiceBroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(MainActivity.this);
        Log.d("wLog", "startActivity");

        Intent intentRequestService = new Intent(this,RequestService.class);
        startService(intentRequestService);

        receiver = new RequestServiceBroadcastReceiver();
        IntentFilter filter = new IntentFilter(RequestService.ACTION_REQUESTSERVICE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

        private List<DayTemp> dayTemps;

        public RecyclerViewAdapter(List<DayTemp> dayTemps) {
            this.dayTemps = dayTemps;
        }

        /**
         * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
         */
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout, viewGroup, false);
            return new ViewHolder(v);
        }

        /**
         * Заполнение виджетов View данными из элемента списка с номером i
         */
        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            Log.d("wLog", "onBindVH:"+i);
            DayTemp dayTemp = dayTemps.get(i);
            Log.d("wLog", dayTemp.getDate());
            Date date = new Date( Long.parseLong(dayTemp.getDate())*1000L);
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+3"));
            String formattedDate = sdf.format(date);
            viewHolder.date.setText(formattedDate);
            viewHolder.morning.setText("Утро: "+ dayTemp.getTemperature().getMorning().toString());
            viewHolder.day.setText("День: "+dayTemp.getTemperature().getDay().toString());
            viewHolder.evening.setText("Вечер: "+dayTemp.getTemperature().getEvening().toString());
            viewHolder.night.setText("Ночь: "+dayTemp.getTemperature().getNight().toString());
        }

        @Override
        public int getItemCount() {
            return dayTemps.size();
        }

        /**
         * Реализация класса ViewHolder, хранящего ссылки на виджеты.
         */
        class ViewHolder extends RecyclerView.ViewHolder {
            @Bind(R.id.date)
            TextView date;
            @Bind(R.id.morning)
            TextView morning;
            @Bind(R.id.day)
            TextView day;
            @Bind(R.id.evening)
            TextView evening;
            @Bind(R.id.night)
            TextView night;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    public class RequestServiceBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            info = (ForecastInfo) intent.getParcelableExtra(RequestService.EXTRA_KEY_OUT);
            Log.d("wLog", "onRecieve");
            if(info !=null && info.getAnswerCode() == 0) {
                RecyclerViewAdapter adapter = new RecyclerViewAdapter(info.getDayTempList());
                LinearLayoutManager layoutManager = new LinearLayoutManager(App.context);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(layoutManager);
            }
        }
    }
}
