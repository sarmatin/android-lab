package ru.sarmatin.weatherapp.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Антон on 08.03.2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class DayTemp implements Serializable{


    @JsonProperty("dt")
    private String date;

    @JsonProperty("temp")
    private Temperature temperature;

    @JsonCreator
    public DayTemp(@JsonProperty("dt")String date,@JsonProperty("temp") Temperature temperature) {
        this.date = date;
        this.temperature = temperature;
    }

    public Temperature getTemperature() {

        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }




}
