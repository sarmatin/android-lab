package ru.sarmatin.weatherapp.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by antonsarmatin on 10.03.16.
 */
@JsonIgnoreProperties(ignoreUnknown =  true)
public class Temperature implements Serializable {


    @JsonProperty("day")
    private Double day;

    @JsonProperty("night")
    private Double night;

    @JsonProperty("eve")
    private Double evening;

    @JsonProperty("morn")
    private Double morning;

    public Double getDay() {
        return day;
    }

    public void setDay(Double day) {
        this.day = day;
    }

    public Double getNight() {
        return night;
    }

    public void setNight(Double night) {
        this.night = night;
    }

    public Double getEvening() {
        return evening;
    }

    public void setEvening(Double evening) {
        this.evening = evening;
    }

    public Double getMorning() {
        return morning;
    }

    public void setMorning(Double morning) {
        this.morning = morning;
    }

    @JsonCreator
    public Temperature(@JsonProperty("day")Double day,@JsonProperty("night") Double night,@JsonProperty("eve") Double evening,@JsonProperty("morn") Double morning) {
        this.day = new BigDecimal(day).setScale(1, RoundingMode.HALF_UP).doubleValue();
        this.night = new BigDecimal(night).setScale(1, RoundingMode.HALF_UP).doubleValue();
        this.evening =new BigDecimal(evening).setScale(1, RoundingMode.HALF_UP).doubleValue();
        this.morning = new BigDecimal(morning).setScale(1, RoundingMode.HALF_UP).doubleValue();

    }
}
