package ru.sarmatin.weatherapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.util.List;

import retrofit.RetrofitError;
import ru.sarmatin.weatherapp.models.DayTemp;
import ru.sarmatin.weatherapp.models.ForecastInfo;
import ru.sarmatin.weatherapp.network.SessionRestManager;

/**
 * Created by antonsarmatin on 10.03.16.
 */
public class RequestService extends IntentService{

    public static final String ACTION_REQUESTSERVICE = "requestService";
    public static final String EXTRA_KEY_OUT = "requestServiceOut";


    public RequestService() {
        super("serviceForecast");
    }

    public void onCreate() {
        super.onCreate();
        Log.d("wLog","startService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ForecastInfo forecastInfo = new ForecastInfo();
        try{
        forecastInfo = SessionRestManager.getInstance().getRest().getForecastByCityId("551487","metric", "7");
        } catch (RetrofitError error) {
            switch (error.getKind()) {
                case NETWORK:
                    forecastInfo.setAnswerCode(1);
                    break;
                case HTTP:
                    forecastInfo.setAnswerCode(2);
                    break;
                case CONVERSION:
                    forecastInfo.setAnswerCode(3);
                    break;
                case UNEXPECTED:
                    forecastInfo.setAnswerCode(4);
                    break;
            }
        }finally {
            Intent response = new Intent();
            response.setAction(ACTION_REQUESTSERVICE);
            response.addCategory(Intent.CATEGORY_DEFAULT);
            response.putExtra(EXTRA_KEY_OUT, forecastInfo);
            Log.d("wLog","requestService");
            sendBroadcast(response);
        }

    }
}
