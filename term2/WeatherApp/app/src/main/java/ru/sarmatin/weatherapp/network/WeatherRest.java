package ru.sarmatin.weatherapp.network;
import retrofit.http.GET;
import retrofit.http.Query;
import ru.sarmatin.weatherapp.models.ForecastInfo;

public interface WeatherRest {
//551487
    @GET("/data/2.5/forecast/daily")
    ForecastInfo getForecastByCityId(@Query("id") String cityId,@Query("units") String units, @Query("cnt") String days);




}