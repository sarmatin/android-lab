package ru.sarmatin.weatherapp;

public class Config {
    public static final String WEATHER_ENDPOINT_DEBUG = "http://api.openweathermap.org/";
    public static final String WEATHER_ENDPOINT_RELEASE = WEATHER_ENDPOINT_DEBUG;

    public static final String APPLICATION_ID = "1b9e76fe25d957f861a72ddae1020f26";
}