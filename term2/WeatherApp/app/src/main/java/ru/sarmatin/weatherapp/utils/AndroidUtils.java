package ru.sarmatin.weatherapp.utils;

import ru.sarmatin.weatherapp.BuildConfig;
import ru.sarmatin.weatherapp.Config;

public final class AndroidUtils {

    private AndroidUtils() {
    }

    public static String getRestEndpoint() {
        return BuildConfig.DEBUG ? Config.WEATHER_ENDPOINT_DEBUG : Config.WEATHER_ENDPOINT_RELEASE;
    }
}