package ru.sarmatin.homework2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Антон on 14.10.2015.
 */
public class NewsAdapter extends BaseAdapter {
    Context context;
    ArrayList<News> arrayList;

    public NewsAdapter(Context context, ArrayList<News> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item, parent, false);

        TextView headerText =  (TextView) rowView.findViewById(R.id.headerText);
        TextView dateText = (TextView) rowView.findViewById(R.id.dateText);
        TextView textView = (TextView) rowView.findViewById(R.id.textView);

        News news = arrayList.get(position);

        headerText.setText(news.getHeader());
        dateText.setText(news.getDate());
        textView.setText(news.getText());

        return rowView;
    }
}
