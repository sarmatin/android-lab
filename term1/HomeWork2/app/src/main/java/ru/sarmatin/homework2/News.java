package ru.sarmatin.homework2;

import android.graphics.drawable.Drawable;
import android.media.Image;

/**
 * Created by Антон on 14.10.2015.
 */
public class News {
    private String header;
    private String date;
    private String text;

    public News(String header, String date, String text) {
        this.header = header;
        this.date = date;
        this.text = text;
    }

    public String getHeader() {
        return header;
    }

    public String getDate() {
        return date;
    }

    public String getText() {
        return text;
    }


}
