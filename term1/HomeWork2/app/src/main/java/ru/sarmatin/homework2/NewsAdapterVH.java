package ru.sarmatin.homework2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Антон on 14.10.2015.
 */
public class NewsAdapterVH extends BaseAdapter {
    Context context;
    ArrayList<News> arrayList;

    public NewsAdapterVH(Context context, ArrayList<News> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    class ViewHolder{
        public TextView header;
        public TextView date;
        public TextView text;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if(rowView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item, parent, false);

            ViewHolder viewHolder = new ViewHolder();

            viewHolder.header = (TextView) rowView.findViewById(R.id.headerText);
            viewHolder.date = (TextView) rowView.findViewById(R.id.dateText);
            viewHolder.text = (TextView) rowView.findViewById(R.id.textView);
            rowView.setTag(viewHolder);
        }

        ViewHolder viewHolder = (ViewHolder) rowView.getTag();

        News news = arrayList.get(position);
        viewHolder.header.setText(news.getHeader());
        viewHolder.date.setText(news.getDate());
        viewHolder.text.setText(news.getText());

        return rowView;
    }
}