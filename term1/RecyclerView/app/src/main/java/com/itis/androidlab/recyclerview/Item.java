package com.itis.androidlab.recyclerview;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

    private String mHeader;
    private String infoHeader;
    private String text;

    public Item(String mHeader, String infoHeader, String text) {
        this.mHeader = mHeader;
        this.infoHeader = infoHeader;
        this.text = text;
    }

    protected Item(Parcel in) {
        mHeader = in.readString();
        infoHeader = in.readString();
        text = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mHeader);
        dest.writeString(infoHeader);
        dest.writeString(text);
    }

    public String getHeader() {
        return mHeader;
    }

    public String getInfoHeader() {
        return infoHeader;
    }

    public String getText() {
        return text;
    }
}