package com.itis.androidlab.recyclerview;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        setupRecyclerView(mRecyclerView);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new RecyclerViewAdapter(Items.getList()));
        Log.d("sLog", Items.getList().get(0).getHeader() );
    }


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolderItem> {
        private List<Item> items;

        public RecyclerViewAdapter(List<Item> items) {
            this.items = items;
        }

        public class ViewHolderItem extends RecyclerView.ViewHolder {

            public final View mView;
            public final TextView mHeader;
            public final TextView mInfo;
            public final TextView mText;

            public ViewHolderItem(View view) {
                super(view);
                mView = view;
                mHeader = (TextView) view.findViewById(R.id.mainHeader);
                mInfo = (TextView) view.findViewById(R.id.infoHeader);
                mText = (TextView) view.findViewById(R.id.textView);
            }
        }

        @Override
        public ViewHolderItem onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
            return new ViewHolderItem(v);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public void onBindViewHolder(ViewHolderItem viewHolderItem, int i) {
            final Item item = items.get(i);
            Log.d("sLog","from Items.getList: "+ Items.getList().get(i).getHeader() );
            Log.d("sLog","from items.get(i)  "+ item.getHeader() );
            Log.d("sLog", "i: "+ String.valueOf(i));

            viewHolderItem.mHeader.setText(item.getHeader());
            viewHolderItem.mInfo.setText(item.getInfoHeader());
            viewHolderItem.mText.setText(item.getText());

            viewHolderItem.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                    intent.putExtra("parcel", item);
                    startActivity(intent);
                }
            });
        }


    }
}
