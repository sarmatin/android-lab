package com.itis.androidlab.recyclerview;

/**
 * Created by Антон on 26.10.2015.
 */
public class TheoryAnswers {
    private String answerOne = "ListView - вью в виде списка, позволяет избежать многократного однотипного прописывания отдельных вью. " +
            "VH позволяет хранить данные о View, а не каждый раз искать их по id." +
            " RecyclerView принуждает использовать VH в отличии от LV, а вообще его развитие.";
    private String answerTwo = "Отличия padding и margin: " +
            "padding - отступ содержимого от края элемента(вью), т.е. внутренний отступ." +
            "margin - отступ самого элемента от краев/других элементов";

}
