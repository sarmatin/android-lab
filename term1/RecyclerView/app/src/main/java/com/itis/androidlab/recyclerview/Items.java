package com.itis.androidlab.recyclerview;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Items {

   private static List<Item> items;

    private static void setData(){
        items = new ArrayList<>();
        items.add(new Item("Brazil","Южная Америка","Футбол, реки и лес."));
        items.add(new Item("USA","Северная Америка","Афроамериканцы, интернеты и гугл."));
        items.add(new Item("Китай","Азия","Алиэкспресс."));
        items.add(new Item("Россия","Европа","Медведи, водка и ушанки."));
        items.add(new Item("Италия","Европа","Пицца!"));
        items.add(new Item("Австралия","Австралия","Змеи и кенгуру."));
    }

    public static List<Item> getList(){
        setData();
        return items;
    }


}
