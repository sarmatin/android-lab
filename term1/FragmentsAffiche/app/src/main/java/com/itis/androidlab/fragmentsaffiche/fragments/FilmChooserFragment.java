package com.itis.androidlab.fragmentsaffiche.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.models.Film;

import java.io.IOException;
import java.util.List;

public class FilmChooserFragment extends Fragment{

    private List<Film> mFilms;
    private RecyclerView mRecyclerView;
    private SharedPreferences mSharedPreferences;

    private static final String PREF_NAME = "prefs";
    RecyclerViewAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_chooser, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        mSharedPreferences = this.getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        mFilms = readFilmsFromJson();
        adapter = new RecyclerViewAdapter(mFilms);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mRecyclerView.getContext()));
        mRecyclerView.setAdapter(adapter);
        return view;
    }


    private List<Film> readFilmsFromJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Film.FilmArray filmArray = mapper.readValue(getActivity().getAssets().open("films.json"),
                    new TypeReference<Film.FilmArray>() {
                    });
            return filmArray.getItems();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

//    @Override
//    public void returnWanted() {
//        ;
//    }

    public interface FilmChooserProcessor {
        void onFilmChosen(Film film, View view);
    }



    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolderItem> {
        private List<Film> films;

        public RecyclerViewAdapter(List<Film> items) {
            this.films = items;
        }

        public class ViewHolderItem extends RecyclerView.ViewHolder {

            public final View mView;
            public final TextView mHeader;

            public ViewHolderItem(View view) {
                super(view);
                mView = view;
                mHeader = (TextView) view.findViewById(R.id.header);
            }
        }

        @Override
        public ViewHolderItem onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_list, viewGroup, false);
            return new ViewHolderItem(v);
        }

        @Override
        public int getItemCount() {
            return films.size();
        }

        @Override
        public void onBindViewHolder(ViewHolderItem viewHolderItem, int i) {
            final Film film = films.get(i);
            viewHolderItem.mHeader.setText(film.getTitle());
            if(mSharedPreferences.getBoolean(film.getTitle(),false)){
                viewHolderItem.mHeader.setBackgroundColor(getResources().getColor(R.color.highlited));
            }else{
                viewHolderItem.mHeader.setBackgroundColor(getResources().getColor(R.color.normal));
            }
            viewHolderItem.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FilmChooserProcessor listener = (FilmChooserProcessor) getActivity();
                    listener.onFilmChosen(film, v);
                }
            });
        }



    }
}
