package com.itis.androidlab.fragmentsaffiche.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.itis.androidlab.fragmentsaffiche.R;
import com.itis.androidlab.fragmentsaffiche.models.Film;

public class FilmDetailsFragment extends Fragment {

    private TextView mTitleTextView;
    private TextView mDateTextView;
    private TextView mDescriptionTextView;
    private TextView mDirectorTextView;
    private TextView mActorsTextView;
    private TextView mBudgetTextView;
    private Button mWantedButton;

    private TextView outFragmentView;

    private SharedPreferences mSharedPreferences;
    private static final String PREF_NAME = "prefs";
    private boolean wanted;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_details, container, false);
        initViews(view);
        mSharedPreferences = this.getActivity().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        Log.d("LOG", String.valueOf(wanted));
        return view;
    }

//    public interface FilmWanted {
//        void returnWanted();
//    }


    private void initViews(View view) {
        mTitleTextView = (TextView) view.findViewById(R.id.film_title);
        mDateTextView = (TextView) view.findViewById(R.id.film_date);
        mDescriptionTextView = (TextView) view.findViewById(R.id.film_description);
        mDirectorTextView = (TextView) view.findViewById(R.id.film_director);
        mActorsTextView = (TextView) view.findViewById(R.id.film_actors);
        mBudgetTextView = (TextView) view.findViewById(R.id.film_budget);
        mWantedButton = (Button) view.findViewById(R.id.wanted);
        mWantedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wanted) {
                    mSharedPreferences.edit().clear();
                    mSharedPreferences.edit().putBoolean(mTitleTextView.getText().toString(), false).apply();
                    outFragmentView.setBackgroundColor(getResources().getColor(R.color.normal));
                    wanted = false;
                } else {
                    mSharedPreferences.edit().clear();
                    mSharedPreferences.edit().putBoolean(mTitleTextView.getText().toString(), true).apply();
                    outFragmentView.setBackgroundColor(getResources().getColor(R.color.highlited));
                    wanted = true;

                }
//                FilmWanted listener = (FilmWanted) getActivity();
//                listener.returnWanted();
            }
        });
    }


    public void setFilm(Film film, View view) {
        mTitleTextView.setText(film.getTitle());
        mDateTextView.setText(film.getDate());
        mDescriptionTextView.setText(film.getDescription());
        mDirectorTextView.setText(String.format(getResources().getString(R.string.film_director), film.getDirector()));
        mActorsTextView.setText(film.getActors());
        mBudgetTextView.setText(film.getBudget());
        wanted = mSharedPreferences.getBoolean(mTitleTextView.getText().toString(), false);
        outFragmentView = (TextView) view.findViewById(R.id.header);
    }
}
